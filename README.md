# I3

Yet another `i3` configuration.

## Usage

### Install

    git clone --recurse-submodules git@gitlab.com:pcoves/i3.git
    stow --target ~ i3

### Uninstall

    stow --delete --target ~ i3
    rm -rf i3
